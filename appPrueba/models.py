from django.db import models
from django.contrib.auth.models import User

class Imagen(models.Model):
	nickname = models.CharField(max_length = 50)
	nombre = models.CharField(max_length = 50)
	foto = models.FileField()
	votos = models.IntegerField(default = 0)
	angulo = models.IntegerField(default = 0)
	flip = models.BooleanField(default = False)
	def __str__(self):
		return '{} - {}'.format(self.nickname, self.nombre)

class Votante(models.Model):
	usuario = models.ForeignKey(User, on_delete = models.CASCADE)
	def __str__(self):
		return self.usuario.username