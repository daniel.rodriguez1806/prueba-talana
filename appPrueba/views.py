from .forms import *
from .models import *
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User, Group
from django.db import transaction
from django.db.models import Q, ProtectedError
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render, redirect
from wsgiref.util import FileWrapper
import os

dir_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

def index(request):
	imagenes = Imagen.objects.all().order_by('-votos')
	if(not request.user.is_authenticated):
		return render(request, 'appPrueba/index.html', {'imagenes': imagenes})
	else:
		try:
			Votante.objects.get(usuario = request.user)
			voto = 1
		except Exception as e:
			voto = 0
		return render(request, 'appPrueba/index.html', {'imagenes': imagenes, 'voto': voto})

def cargarImagen(request):
	if(request.method == 'POST'):
		nickname = request.POST.get('nickname')
		nombre = request.POST.get('nombre')
		angulo = request.POST.get('angle')
		flip = False
		if request.POST.get('flip') == 1 or request.POST.get('flip') == '1':
			flip = True
		foto = request.FILES['foto']

		imagen = Imagen()
		imagen.nickname = nickname
		imagen.nombre = nombre
		imagen.angulo = angulo
		imagen.flip = flip
		imagen.foto = foto
		imagen.save()

		return redirect('index')

	return render(request, 'appPrueba/cargarImagen.html')

@login_required(login_url='iniciarSesion')
def cerrarSesion(request):

	logout(request)
	return redirect('index')

def iniciarSesion(request):

	if not request.user.is_authenticated:

		if request.method == 'POST':
			usuario = request.POST.get("username")
			password = request.POST.get("password")

			user = authenticate(request, username=(usuario), password=password)
			if user is not None:
				if user.is_active:
					login(request, user)
					return redirect('index')

	return redirect('index')

def registrarUsuario(request):

	if(request.method == 'POST'):
		idUsuario = request.POST.get('idUsuario')
		password = request.POST.get('password')

		try:
			with transaction.atomic():
				dataUser = {
					'username': idUsuario,
					'password': password
				}

				user_form = UserForm(dataUser)

				if user_form.is_valid():
					user = user_form.save()
					user.set_password(user.password)
					user.save()

					return redirect('index')

				else:
					status = 2
					print("ya hay un usuario con esa username")

		except Exception as e:
			print(e)
			status = -1

	return render(request, 'appPrueba/registrarUsuario.html')

@login_required(login_url='iniciarSesion')
def sumarVoto(request):
	try:
		with transaction.atomic():
			idImagen = request.GET.get('idImagen')
			imagen = Imagen.objects.get(id = idImagen)
			imagen.votos = imagen.votos + 1
			imagen.save()

			votante = Votante()
			votante.usuario = request.user
			votante.save()

			return JsonResponse({'n': imagen.votos, 'status': 1})

	except Exception as e:
		print(e)
		return JsonResponse({'status': -1})

def downloadFile(request, archivo):

	wrapper = FileWrapper(open(os.path.join(dir_path, 'media/'+archivo), 'rb'))
	print("wrapper:", wrapper)
	response = HttpResponse(wrapper, content_type='application/force-download')
	response['Content-Disposition'] = 'inline; filename=' + os.path.basename(archivo)
	return response
