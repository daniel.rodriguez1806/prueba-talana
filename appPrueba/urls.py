from django.contrib import admin
from django.urls import path

from . import views

urlpatterns = [
	path('', views.index, name='index'),
	path('cargarImagen', views.cargarImagen, name='cargarImagen'),
	path('iniciarSesion', views.iniciarSesion, name='iniciarSesion'),
	path('cerrarSesion', views.cerrarSesion, name='cerrarSesion'),
	path('registrarUsuario', views.registrarUsuario, name='registrarUsuario'),
	path('sumarVoto', views.sumarVoto, name='sumarVoto'),
	path('media/<archivo>', views.downloadFile, name='downloadFile')
]
